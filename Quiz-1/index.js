//soal 1

function next_date(tanggal,bulan,tahun){
    tanggal += 1
    if( tanggal > 31 && bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12){
        bulan += 1 
        tanggal -= 31
    }
    if( tanggal > 30 && bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11){
        bulan += 1
        tanggal -= 30
    }
    if ( tanggal > 28 && bulan == 2){
        bulan += 1
        tanggal -= 28
    }
    if ( bulan > 12){
        bulan -= 12
        tahun += 1
    }
    
    switch(bulan) {
        case 1:   { bulan = "January"; break; }
        case 2:   { bulan = "Februari"; break; }
        case 3:   { bulan = "Maret"; break; }
        case 4:   { bulan = "April"; break; }
        case 5:   { bulan = "Mei"; break; }
        case 6:   { bulan = "Juni"; break; }
        case 7:   { bulan = "Juli"; break; }
        case 8:   { bulan = "Agustus"; break; }
        case 9:   { bulan = "September"; break; }
        case 10:   { bulan = "Oktober"; break; }
        case 11:   { bulan = "November"; break; }
        case 12:   { bulan = "Desember"; break; }
        default:  { bulan = "angka yang anda masukan salah"; }}
    var tanggalLahir = tanggal+" "+bulan+" "+tahun
    console.log(tanggalLahir)
}

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun )

//soal 2
function jumlah_kata(kata){
    kalimat = kata.split(" ")
    var spasi = 0
    for (i=0;i<kalimat.length;i++){
        if (kalimat[i] == ""){
            spasi += 1
        } 
    }
    kalimat1 = kalimat.length - spasi
    console.log(kalimat1)
}
var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal "
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4