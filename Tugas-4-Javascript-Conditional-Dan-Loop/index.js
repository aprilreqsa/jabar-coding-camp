//soal 1
var nilai = 50
var indeks
if (nilai >= 85){
    indeks = "A"
}else if (nilai >= 75 && nilai < 85) {
    indeks = "B"
}else if (nilai >= 65 && nilai < 75){
    indeks = "C"
}else if(nilai >= 55 && nilai < 65) {
    indeks = "D"
}else {
    indeks = "E"
}

console.log("indeks : "+indeks)

//soal 2
var tanggal = 17;
var bulan = 4;
var tahun = 1992;


switch(bulan) {
    case 1:   { bulan = "January"; break; }
    case 2:   { bulan = "Februari"; break; }
    case 3:   { bulan = "Maret"; break; }
    case 4:   { bulan = "April"; break; }
    case 5:   { bulan = "Mei"; break; }
    case 6:   { bulan = "Juni"; break; }
    case 7:   { bulan = "Juli"; break; }
    case 8:   { bulan = "Agustus"; break; }
    case 9:   { bulan = "September"; break; }
    case 10:   { bulan = "Oktober"; break; }
    case 11:   { bulan = "November"; break; }
    case 12:   { bulan = "Desember"; break; }
    default:  { bulan = "angka yang anda masukan salah"; }}
var tanggalLahir = tanggal+" "+bulan+" "+tahun
console.log(tanggalLahir)

//soal 3
n=7
var deret = ''
for(i=0;i<=n;i++){  
    for(j=0;j<i;j++){
         deret += '#'       
    } 
    deret += '\n'
}
console.log(deret)

//soal 4
var m = 10
var samadengan = ''
for (i=1;i<=m;i++){
    samadengan += '=' 
    if (i % 3 == 0){
        console.log(i +" - I love VueJS \n" + samadengan )
    } else if(i % 3 == 2){
        console.log(i +" - I love Javascript")
    }else if(i % 3 == 1){
     console.log(i +" - I love programming")
    }
     
 }
