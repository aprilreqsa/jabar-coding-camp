// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var ketiga = pertama.substr(0,4).concat(pertama.substr(11,8)).concat(kedua.substr(0,8)).concat(kedua.substr(8,10).toUpperCase())

console.log(ketiga)

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angka1 = parseInt(kataPertama)
var angka2 = parseInt(kataKedua)
var angka3 = parseInt(kataKetiga)
var angka4 = parseInt(kataKeempat)
var output = (angka1+angka4)+(angka2*angka3)
console.log(output)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);